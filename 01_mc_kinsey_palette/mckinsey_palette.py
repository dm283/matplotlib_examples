# coding=utf-8

#<code_cell id="0cadc649d34ac24fad8806b8e1fc69badfc54bbeb771c788dae2c74b260df0e6">
# ПАЛИТРА ЦВЕТОВ MCKINSEY
# Основные цвета
deepblue='#051C2C'; 
cyan='#00A9F4'; 
electricblue='#1F40E6'; 
paleblue='#AAE6F0'; 
turquoise='#3C96B4';
paleelectricblue='#AFC3FF';
# Цвета для акцентов
purple='#8C5AC8'; 
pink='#E6A0C8'; 
red='#E5546C'; 
orange='#FAA082';
# Серая палитра от темного к светлому
darkgray='#4D4D4D'; 
midgray='#7F7F7F'; 
lightgray='#B3B3B3'; 
superlightgray='#D0D0D0';
palegray='#E6E6E6'; 
superpalegray='#F0F0F0'; 
offwhite='#F8F8F8'
#</code_cell>

#<code_cell id="e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855">

#</code_cell>

