# coding=utf-8

#<code_cell id="1a02e0f092e65a44f88bfd780d4a97dd66e3ca5995db48cec343c43fe646622b">
import sys
import os

sys.path.append(os.getcwd()) # mckinsey.py module is in the folder of this script
import mckinsey_palette as mc
#</code_cell>

#<code_cell id="46db04037f2fbed511f17c5c95f5325b03a913528cdd9451f4158288ac497d90">
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')
import numpy as np
#</code_cell>

#<code_cell id="b14b956e087787d83ea8a2480ffb467f23e9ca95e3a5af4bd8374011d2050017">
clrs = [ [mc.deepblue, mc.cyan, mc.electricblue, mc.paleblue, mc.turquoise, mc.paleelectricblue],
        [mc.purple, mc.pink, mc.red, mc.orange],
        [mc.darkgray, mc.midgray, mc.lightgray, mc.superlightgray, mc.palegray, mc.superpalegray, mc.offwhite] ]

labels = [ ['deepblue', 'cyan', 'electricblue', 'paleblue', 'turquoise', 'paleelectricblue'],
        ['purple', 'pink', 'red', 'orange'],
        ['darkgray', 'midgray', 'lightgray', 'superlightgray', 'palegray', 'superpalegray', 'offwhite'] ]

titles = [ 'Основные цвета',
        'Цвета для акцентов',
        'Серая палитра от темного к светлому' ]

fig, axes = plt.subplots(3, 1, figsize=(5, 17))
fig.set(facecolor='#ffffe6')

for n in range( len(clrs) ):
    axes[n].pie([1]*len(clrs[n]), colors=clrs[n], labels=labels[n])
    axes[n].set(title=titles[n])
    axes[n].title.set(color=mc.electricblue, size='15')
    axes[n].legend(bbox_to_anchor=(1.2, 1), loc='upper left')
    
plt.savefig('mckinsey_palette.png', bbox_inches='tight', facecolor=fig.get_facecolor())
#</code_cell>

#<code_cell id="e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855">

#</code_cell>

