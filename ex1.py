import numpy as np
import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt

x = np.linspace(-3, 3, 200)
y = x * (x + 2) * (x - 2)

fig, ax = plt.subplots()
ax.plot(x, y)

plt.show()
